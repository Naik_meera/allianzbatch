package com.metamorphosys.allianceWeb;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(scanBasePackages={"com.metamorphosys"})
@ImportResource({ "classpath:job-context.xml" })
public class AlliancewebApplication extends SpringBootServletInitializer  {

	public static void main(String[] args) {
		ApplicationContext ctx = (ApplicationContext) SpringApplication.run(AlliancewebApplication.class, args);
	}
	
	 @Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	        return application.sources(AlliancewebApplication.class);
	    }
	 @Bean
	  public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager() {
	    PoolingHttpClientConnectionManager result = new PoolingHttpClientConnectionManager();
	    result.setMaxTotal(20);
	    return result;
	  }

	  @Bean
	  public RequestConfig requestConfig() {
	    RequestConfig result = RequestConfig.custom()
	      .setConnectionRequestTimeout(200000)
	      .setConnectTimeout(200000)
	      .setSocketTimeout(200000)
	      .build();
	    return result;
	  }

	  @Bean
	  public CloseableHttpClient httpClient(PoolingHttpClientConnectionManager poolingHttpClientConnectionManager, RequestConfig requestConfig) {
	    CloseableHttpClient result = HttpClientBuilder
	      .create()
	      .setConnectionManager(poolingHttpClientConnectionManager)
	      .setDefaultRequestConfig(requestConfig)
	      .build();
	    return result;
	  }

	  @Bean
	  public RestTemplate restTemplate(HttpClient httpClient) {
	    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
	    requestFactory.setHttpClient(httpClient);
	    requestFactory.setBufferRequestBody(false);
	    return new RestTemplate(requestFactory);
	  }
}
