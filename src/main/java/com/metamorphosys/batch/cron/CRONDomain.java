package com.metamorphosys.batch.cron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.batch.dataobjects.SchedulerDO;
import com.metamorphosys.batch.dataobjects.SchedulerRepository;

@Component("CRONDomain")
public class CRONDomain {
	
	@Autowired
	private SchedulerRepository schedulerRepository;
	
	public String fetchExpressionForBatch(String schedulerCd){
		SchedulerDO schedulerDO=schedulerRepository.findBySchedulerCd(schedulerCd);
		if(schedulerDO!=null){
			return schedulerDO.getCronExpression();
		}
		return null;
	}

}
