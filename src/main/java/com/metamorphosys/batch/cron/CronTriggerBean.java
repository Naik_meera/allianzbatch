package com.metamorphosys.batch.cron;

import java.text.ParseException;
import java.util.List;

import org.springframework.scheduling.quartz.CronTriggerFactoryBean;

import com.metamorphosys.batch.dataobjects.BatchDO;
import com.metamorphosys.batch.dataobjects.BatchRepository;

public class CronTriggerBean extends CronTriggerFactoryBean {

	private String batchName;
	private CRONDomain CRONDomain;
	private BatchRepository batchRepository;

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public CRONDomain getCRONDomain() {
		return CRONDomain;
	}

	public void setCRONDomain(CRONDomain cRONDomain) {
		CRONDomain = cRONDomain;
	}

	public BatchRepository getBatchRepository() {
		return batchRepository;
	}

	public void setBatchRepository(BatchRepository batchRepository) {
		this.batchRepository = batchRepository;
	}

	@Override
	public void afterPropertiesSet() throws ParseException {
		//BatchDO batchDOs = batchRepository.findBySchedulerCd(this.batchName);
		String cronExpression = this.CRONDomain.fetchExpressionForBatch(this.batchName);
		this.setCronExpression(cronExpression);
		super.afterPropertiesSet();
	}

}
