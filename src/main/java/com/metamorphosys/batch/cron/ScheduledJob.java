package com.metamorphosys.batch.cron;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.metamorphosys.insureconnect.jpa.transaction.UserRepository;

public class ScheduledJob extends QuartzJobBean {

	private UserRepository userRepository;
	

	public UserRepository getUserRepository() {
		return userRepository;
	}


	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}


	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
	}

}