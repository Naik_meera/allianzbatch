package com.metamorphosys.batch.dataobjects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BATCH")
public class BatchDO {

	@Id
	private Long id;
	private String batchCd;
	private String batchName;
	private String schedulerCd;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBatchCd() {
		return batchCd;
	}
	public void setBatchCd(String batchCd) {
		this.batchCd = batchCd;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public String getSchedulerCd() {
		return schedulerCd;
	}
	public void setSchedulerCd(String schedulerCd) {
		this.schedulerCd = schedulerCd;
	}
	
}
