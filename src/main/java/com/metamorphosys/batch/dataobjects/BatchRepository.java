package com.metamorphosys.batch.dataobjects;

import org.springframework.data.repository.CrudRepository;

public interface BatchRepository extends CrudRepository<BatchDO, Long> {

}
