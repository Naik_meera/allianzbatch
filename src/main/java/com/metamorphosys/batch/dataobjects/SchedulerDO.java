package com.metamorphosys.batch.dataobjects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SCHEDULER")
public class SchedulerDO {

	@Id
	private Long id;
	private String schedulerName;
	private String schedulerCd;
	private String cronExpression;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSchedulerName() {
		return schedulerName;
	}
	public void setSchedulerName(String schedulerName) {
		this.schedulerName = schedulerName;
	}
	public String getSchedulerCd() {
		return schedulerCd;
	}
	public void setSchedulerCd(String schedulerCd) {
		this.schedulerCd = schedulerCd;
	}
	public String getCronExpression() {
		return cronExpression;
	}
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
}
