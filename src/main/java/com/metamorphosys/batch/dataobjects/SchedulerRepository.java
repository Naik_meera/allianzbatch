package com.metamorphosys.batch.dataobjects;

import org.springframework.data.repository.CrudRepository;

public interface SchedulerRepository extends CrudRepository<SchedulerDO, Long> {

	SchedulerDO findBySchedulerCd(String schedulerCd);
}
