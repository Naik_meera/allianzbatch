/*package com.metamorphosys.batch.jobs;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.metamorphosys.insureconnect.dataobjects.master.AccidentHealthClub;
import com.metamorphosys.insureconnect.dataobjects.master.AutoClub;
import com.metamorphosys.insureconnect.dataobjects.master.CommercialClub;
import com.metamorphosys.insureconnect.dataobjects.master.GwpGraph;
import com.metamorphosys.insureconnect.dataobjects.master.IncomeChart;
import com.metamorphosys.insureconnect.dataobjects.master.LiabilitySurprise;
import com.metamorphosys.insureconnect.dataobjects.master.LoBSummary;
import com.metamorphosys.insureconnect.dataobjects.master.NewBusinessBonus;
import com.metamorphosys.insureconnect.dataobjects.master.OfficeAllowance;
import com.metamorphosys.insureconnect.dataobjects.master.PerformanceBonus;
import com.metamorphosys.insureconnect.dataobjects.master.RenewalBusinessBonus;
import com.metamorphosys.insureconnect.dataobjects.master.TravelClub;
import com.metamorphosys.insureconnect.jpa.master.AccidentHealthClubRepository;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.jpa.master.AutoClubRepository;
import com.metamorphosys.insureconnect.jpa.master.CommercialClubRepository;
import com.metamorphosys.insureconnect.jpa.master.GwpGraphRepository;
import com.metamorphosys.insureconnect.jpa.master.IncomeChartRepository;
import com.metamorphosys.insureconnect.jpa.master.LiabilitySurpriseRepository;
import com.metamorphosys.insureconnect.jpa.master.LoBSummaryRepository;
import com.metamorphosys.insureconnect.jpa.master.NewBusinessBonusRepository;
import com.metamorphosys.insureconnect.jpa.master.OfficeAllowanceRepository;
import com.metamorphosys.insureconnect.jpa.master.PerformanceBonusRepository;
import com.metamorphosys.insureconnect.jpa.master.RenewalBusinessBonusRepository;
import com.metamorphosys.insureconnect.jpa.master.TravelClubRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class DashboardExcelReadingJob extends QuartzJobBean {
	
	private static final Logger log = LoggerFactory.getLogger(DashboardExcelReadingJob.class);
	
	GwpGraphRepository gwpGraphRepository;

	IncomeChartRepository incomeChartRepository;

	NewBusinessBonusRepository newBusinessBonusRepository;

	PerformanceBonusRepository performanceBonusRepository;
	
	OfficeAllowanceRepository officeAllowanceRepository;

	RenewalBusinessBonusRepository renewalBusinessBonusRepository;

	AutoClubRepository autoClubRepository;
	
	CommercialClubRepository commercialClubRepository;

	LiabilitySurpriseRepository liabilitySurpriseRepository;

	AccidentHealthClubRepository accidentHealthClubRepository;

	TravelClubRepository travelClubRepository;
	
	LoBSummaryRepository loBSummaryRepository;
	
	ApplicationRepository applicationRepository;
	
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		log.info("DashboardExcelReadingJob started execution at  " + new Date());
		
		String url =applicationRepository.findByKey(InsureConnectConstants.Path.DASHBOARDFILEPATH).getValue();
		Workbook workbook = null;
		try {

			workbook = Workbook.getWorkbook(new File(url));
			Sheet sheet = workbook.getSheet(0);
			// Cell cell1 = sheet.getCell(column, row);
			deleteRecords();
			log.info("Records deleted");
			int rows = sheet.getRows();
			log.info("Number of rows in excel"+rows);
			for (int row = 3; row < rows; row++) {
				int column = 0;
				Cell cell1 = sheet.getCell(column++, row);
				System.out.print("agent id"+cell1.getContents());
				String agentId = cell1.getContents();
				cell1 = sheet.getCell(column++, row);
				String agentName = cell1.getContents();
				GwpGraph gwpGraph = new GwpGraph();
				gwpGraph.setAgentId(agentId);
				gwpGraph.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				gwpGraph.setCurrYrGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				gwpGraph.setPrevYrGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				gwpGraph.setCurrYrRenewalGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				gwpGraph.setPrevYrRenewalGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				gwpGraph.setCurrYrNewBizGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				gwpGraph.setPrevYrNewBizGWP(cell1.getContents());
				gwpGraphRepository.save(gwpGraph);

				IncomeChart incomeChart = new IncomeChart();
				incomeChart.setAgentId(agentId);
				incomeChart.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrTotalIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrTotalIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrCommission(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrCommission(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrCashBonus(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrCashBonus(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrJanIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrFebIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrMarIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrAprIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrMayIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrJulIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrJulIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrAugIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrSepIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrOctIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrNovIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setCurrYrDecIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrJanIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrFebIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrMarIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrAprIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrMayIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrJunIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrJulIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrAugIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrSepIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrOctIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrNovIncome(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				incomeChart.setPrevYrDecIncome(cell1.getContents());
				incomeChartRepository.save(incomeChart);

				PerformanceBonus performanceBonus = new PerformanceBonus();
				performanceBonus.setAgentId(agentId);
				performanceBonus.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				performanceBonus.setCurrYrGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				performanceBonus.setCurrYrGrowth(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				performanceBonus.setCurrYrLossRatio(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				performanceBonus.setCurrYrEstBonus(cell1.getContents());
				performanceBonusRepository.save(performanceBonus);

				OfficeAllowance officeAllowance = new OfficeAllowance();
				officeAllowance.setAgentId(agentId);
				officeAllowance.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				officeAllowance.setCurrYrGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				officeAllowance.setCurrYrGrowth(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				officeAllowance.setCurrYrEstBonus(cell1.getContents());
				officeAllowanceRepository.save(officeAllowance);

				NewBusinessBonus newBusinessBonus = new NewBusinessBonus();
				newBusinessBonus.setAgentId(agentId);
				newBusinessBonus.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				newBusinessBonus.setCurrYrNewBizGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				newBusinessBonus.setCurrYrLRMotor(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				newBusinessBonus.setCurrYrLRProperty(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				newBusinessBonus.setCurrYrLRLiability(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				newBusinessBonus.setCurrYrLRTravel(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				newBusinessBonus.setCurrYrLRPA(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				newBusinessBonus.setCurrYrLRMarine(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				newBusinessBonus.setCurrYrLREng(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				newBusinessBonus.setCurrYrEstBonus(cell1.getContents());
				newBusinessBonusRepository.save(newBusinessBonus);

				RenewalBusinessBonus renewalBusinessBonus = new RenewalBusinessBonus();
				renewalBusinessBonus.setAgentId(agentId);
				renewalBusinessBonus.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				renewalBusinessBonus.setCurrYrGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				renewalBusinessBonus.setCurrYrRenewalGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				renewalBusinessBonus.setCurrYrLRMotor(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				renewalBusinessBonus.setCurrYrLRProperty(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				renewalBusinessBonus.setCurrYrLRLiability(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				renewalBusinessBonus.setCurrYrLRTravel(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				renewalBusinessBonus.setCurrYrLRPA(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				renewalBusinessBonus.setCurrYrLRMarine(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				renewalBusinessBonus.setCurrYrLREng(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				renewalBusinessBonus.setCurrYrEstBonus(cell1.getContents());
				renewalBusinessBonusRepository.save(renewalBusinessBonus);

				AutoClub autoClub = new AutoClub();
				autoClub.setAgentId(agentId);
				autoClub.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPJan(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPFeb(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPMar(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPApr(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPMay(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPJun(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPJul(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPAug(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPSep(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPOct(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPNov(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrAutoGWPDec(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				autoClub.setCurrYrEstBonus(cell1.getContents());
				autoClubRepository.save(autoClub);

				CommercialClub commercialClub = new CommercialClub();
				commercialClub.setAgentId(agentId);
				commercialClub.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPJan(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPFeb(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPMar(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPApr(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPMay(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPJun(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPJul(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPAug(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPSep(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPOct(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPNov(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrCommGWPDec(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				commercialClub.setCurrYrEstBonus(cell1.getContents());

				commercialClubRepository.save(commercialClub);

				LiabilitySurprise liabilitySurprise = new LiabilitySurprise();
				liabilitySurprise.setAgentId(agentId);
				liabilitySurprise.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				liabilitySurprise.setCurrYrPolicies(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				liabilitySurprise.setCurrYrQualifyingGWP(cell1.getContents());
				liabilitySurpriseRepository.save(liabilitySurprise);

				AccidentHealthClub accidentHealthClub = new AccidentHealthClub();
				accidentHealthClub.setAgentId(agentId);
				accidentHealthClub.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				accidentHealthClub.setCurrYrQualifyingGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				accidentHealthClub.setCurrYrEstBonus(cell1.getContents());
				accidentHealthClubRepository.save(accidentHealthClub);

				TravelClub travelClub = new TravelClub();
				travelClub.setAgentId(agentId);
				travelClub.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				travelClub.setCurrYrQualifyingGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				travelClub.setCurrYrEstBonus(cell1.getContents());
				travelClubRepository.save(travelClub);

				LoBSummary loBSummary = new LoBSummary();
				loBSummary.setAgentId(agentId);
				loBSummary.setAgentName(agentName);
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrMotorGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setPrevYrMotorGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrLRMotor(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrAvgMotorCommission(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrPropertyGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setPrevYrPropertyGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrLRProperty(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrAvgPropertyCommission(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrPAHealthGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setPrevYrPAHealthGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrLRPAHealth(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrAvgPAHCommission(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrTravelGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setPrevYrTravelGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrLRTravel(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrAvgTravelCommission(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrLiabilityGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setPrevYrLiabilityGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrLRLiability(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrAvgLiabCommission(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrMarineGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setPrevYrMarineGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrLRMarine(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrAvgMarineCommission(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrEngGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setPrevYrEngGWP(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrLREng(cell1.getContents());
				cell1 = sheet.getCell(column++, row);
				loBSummary.setCurrYrAvgEngCommission(cell1.getContents());
				loBSummaryRepository.save(loBSummary);
			}
		} catch (IOException e) {
			log.info("File Not Found");
			e.printStackTrace();
		} catch (Exception e) {
			log.info("other error");
			e.printStackTrace();
		} finally {

			if (workbook != null) {
				workbook.close();
			}

		}

	}
	public void deleteRecords() {
		
		gwpGraphRepository.deleteAll();

		incomeChartRepository.deleteAll();

		newBusinessBonusRepository.deleteAll();

		performanceBonusRepository.deleteAll();
		
		officeAllowanceRepository.deleteAll();

		renewalBusinessBonusRepository.deleteAll();

		autoClubRepository.deleteAll();
		
		commercialClubRepository.deleteAll();

		liabilitySurpriseRepository.deleteAll();

		accidentHealthClubRepository.deleteAll();

		travelClubRepository.deleteAll();
		
		loBSummaryRepository.deleteAll();
	}
	
	public GwpGraphRepository getGwpGraphRepository() {
		return gwpGraphRepository;
	}
	public void setGwpGraphRepository(GwpGraphRepository gwpGraphRepository) {
		this.gwpGraphRepository = gwpGraphRepository;
	}
	public IncomeChartRepository getIncomeChartRepository() {
		return incomeChartRepository;
	}
	public void setIncomeChartRepository(IncomeChartRepository incomeChartRepository) {
		this.incomeChartRepository = incomeChartRepository;
	}
	public NewBusinessBonusRepository getNewBusinessBonusRepository() {
		return newBusinessBonusRepository;
	}
	public void setNewBusinessBonusRepository(NewBusinessBonusRepository newBusinessBonusRepository) {
		this.newBusinessBonusRepository = newBusinessBonusRepository;
	}
	public PerformanceBonusRepository getPerformanceBonusRepository() {
		return performanceBonusRepository;
	}
	public void setPerformanceBonusRepository(PerformanceBonusRepository performanceBonusRepository) {
		this.performanceBonusRepository = performanceBonusRepository;
	}
	public OfficeAllowanceRepository getOfficeAllowanceRepository() {
		return officeAllowanceRepository;
	}
	public void setOfficeAllowanceRepository(OfficeAllowanceRepository officeAllowanceRepository) {
		this.officeAllowanceRepository = officeAllowanceRepository;
	}
	public RenewalBusinessBonusRepository getRenewalBusinessBonusRepository() {
		return renewalBusinessBonusRepository;
	}
	public void setRenewalBusinessBonusRepository(RenewalBusinessBonusRepository renewalBusinessBonusRepository) {
		this.renewalBusinessBonusRepository = renewalBusinessBonusRepository;
	}
	public AutoClubRepository getAutoClubRepository() {
		return autoClubRepository;
	}
	public void setAutoClubRepository(AutoClubRepository autoClubRepository) {
		this.autoClubRepository = autoClubRepository;
	}
	public CommercialClubRepository getCommercialClubRepository() {
		return commercialClubRepository;
	}
	public void setCommercialClubRepository(CommercialClubRepository commercialClubRepository) {
		this.commercialClubRepository = commercialClubRepository;
	}
	public LiabilitySurpriseRepository getLiabilitySurpriseRepository() {
		return liabilitySurpriseRepository;
	}
	public void setLiabilitySurpriseRepository(LiabilitySurpriseRepository liabilitySurpriseRepository) {
		this.liabilitySurpriseRepository = liabilitySurpriseRepository;
	}
	public AccidentHealthClubRepository getAccidentHealthClubRepository() {
		return accidentHealthClubRepository;
	}
	public void setAccidentHealthClubRepository(AccidentHealthClubRepository accidentHealthClubRepository) {
		this.accidentHealthClubRepository = accidentHealthClubRepository;
	}
	public TravelClubRepository getTravelClubRepository() {
		return travelClubRepository;
	}
	public void setTravelClubRepository(TravelClubRepository travelClubRepository) {
		this.travelClubRepository = travelClubRepository;
	}
	public LoBSummaryRepository getLoBSummaryRepository() {
		return loBSummaryRepository;
	}
	public void setLoBSummaryRepository(LoBSummaryRepository loBSummaryRepository) {
		this.loBSummaryRepository = loBSummaryRepository;
	}
	public ApplicationRepository getApplicationRepository() {
		return applicationRepository;
	}
	public void setApplicationRepository(ApplicationRepository applicationRepository) {
		this.applicationRepository = applicationRepository;
	}
}
*/