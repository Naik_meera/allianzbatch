package com.metamorphosys.batch.jobs;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.ui.velocity.VelocityEngineUtils;

public class EmailJob extends QuartzJobBean {

	private VelocityEngine velocityEngine;
	private JavaMailSender javaMailSender;

	public VelocityEngine getVelocityEngine() {
		return velocityEngine;
	}
	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}


	public JavaMailSender getJavaMailSender() {
		return javaMailSender;
	}
	public void setJavaMailSender(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		Map model = new HashMap();
		model.put("title", "Title");
		model.put("body", "Test");
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "email.vm", "UTF-8", model);
		
		MimeMessage mail = javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper message = new MimeMessageHelper(mail, true);
			message.setTo("swapnil@metamorphtech.com");
			message.setFrom("anna.edzeal@gmail.com");
			message.setSubject("Test Mail");
			message.setText(text, true);
		} catch (MessagingException e) {
			e.printStackTrace();
		} finally {
		}
		javaMailSender.send(mail);
	}

}
