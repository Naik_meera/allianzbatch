	package com.metamorphosys.batch.jobs;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.client.RestTemplate;

import com.metamorphosys.insureconnect.dataobjects.master.DataSyncDetailsDO;
import com.metamorphosys.insureconnect.dataobjects.master.MakeDO;
import com.metamorphosys.insureconnect.dataobjects.master.MakeModelDO;
import com.metamorphosys.insureconnect.dataobjects.master.ModelVariantDO;
import com.metamorphosys.insureconnect.dataobjects.master.SiRegtYearDO;
import com.metamorphosys.insureconnect.dataobjects.master.VehicleMasterDumpDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.jpa.master.DataSyncDetailsRepository;
import com.metamorphosys.insureconnect.jpa.master.MakeModelRepository;
import com.metamorphosys.insureconnect.jpa.master.MakeRepository;
import com.metamorphosys.insureconnect.jpa.master.ModelVariantRepository;
import com.metamorphosys.insureconnect.jpa.master.SiRegtYearRepository;
import com.metamorphosys.insureconnect.jpa.master.VehicleMasterDumpRepository;
import com.metamorphosys.insureconnect.transferobjects.MasterVehicleResponseTO;
import com.metamorphosys.insureconnect.utilities.GuidGenerator;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

public class MasterVehicleSyncJob extends QuartzJobBean {
	
	private static final Logger log = LoggerFactory.getLogger(MasterVehicleSyncJob.class);

	MakeRepository makeRepository;

	ModelVariantRepository modelVariantRepository;

	VehicleMasterDumpRepository vehicleMasterDumpRepository;

	SiRegtYearRepository siRegtYearRepository;
	
	MakeModelRepository makeModelRepository;

	DataSyncDetailsRepository dataSyncDetailsRepository;

	ApplicationRepository applicationRepository;
	
	@Override
	protected void executeInternal(JobExecutionContext paramJobExecutionContext) throws JobExecutionException {
		log.info("Master vehicle sync job started execution at  "+new Date());
		//VehicleMasterDumbDo vehicleMasterDumbDo =new VehicleMasterDumbDo();
		String url =applicationRepository.findByKey(InsureConnectConstants.Path.VEHICLESYNCPATH).getValue();
		
		RestTemplate restTemplate = new RestTemplate();
	
		HttpHeaders httpHeaders = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(httpHeaders);
		log.info("Getting vehicles data from "+url);
		//MasterVehicleResponseTO responseObject = restTemplate.getForObject(url,MasterVehicleResponseTO.class);//.exchange(url,HttpMethod.GET, entity, String.class);
		//log.info("Response "+responseObject);
		//MasterVehicleResponseTO responseTO= (MasterVehicleResponseTO) SerializationUtility.getInstance().fromJson(responseObject.getBody(), MasterVehicleResponseTO.class);
		ResponseEntity<String> responseObject = restTemplate.exchange(url,HttpMethod.GET, entity, String.class);
		//log.info("Response "+responseObject);
		MasterVehicleResponseTO responseTO= (MasterVehicleResponseTO) SerializationUtility.getInstance().fromJson(responseObject.getBody(), MasterVehicleResponseTO.class);
		
		//return responseTO;
		List<VehicleMasterDumpDO> vehicleMasterDump= responseTO.getReplyMessageList();
		
		if(vehicleMasterDump!=null && vehicleMasterDump.size()!=0){
			log.info("Updating "+vehicleMasterDump.size()+" Records");
			for(VehicleMasterDumpDO vehicleMasterDumpDO:vehicleMasterDump){
				try{
					VehicleMasterDumpDO vehicleData=vehicleMasterDumpRepository.findByVehicleCodeAndYear(vehicleMasterDumpDO.getVehicleCode(), vehicleMasterDumpDO.getYear());
					if(vehicleData!=null) {
						vehicleMasterDumpDO.setId(vehicleData.getId());
						vehicleMasterDumpDO.setGuid(vehicleData.getGuid());
					}else {
						vehicleMasterDumpDO.setGuid(GuidGenerator.generate());
					}
					vehicleMasterDumpDO.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
					if(vehicleMasterDumpDO.getVehicleSubtype().equals(VehicleMasterDumpDO.NONTRUCK) && vehicleMasterDumpDO.getVehicleClass().equals(VehicleMasterDumpDO.VEHICLECLASSREGULAR)) {
						vehicleMasterDumpDO.setStatusCd(VehicleMasterDumpDO.ACTIVE);
						addData(vehicleMasterDumpDO);
					}else {
						vehicleMasterDumpDO.setStatusCd(VehicleMasterDumpDO.INACTIVE);
					}
					vehicleMasterDumpRepository.save(vehicleMasterDumpDO);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		List<DataSyncDetailsDO> dataSyncDate=null;
		try{
			dataSyncDate=dataSyncDetailsRepository.findAll();
			for(DataSyncDetailsDO dataSyncDetailsDO:dataSyncDate){
				 dataSyncDetailsDO.setLastUpdatedDate(new Timestamp(System.currentTimeMillis()));
				 dataSyncDetailsRepository.save(dataSyncDetailsDO);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void addData(VehicleMasterDumpDO vehicleMasterDumpDO) {
		try {
			MakeDO makeDO = makeRepository.findByMakeCd(vehicleMasterDumpDO.getBrand());
			if (makeDO != null) {
				// update values
				makeDO.setMakeCd(vehicleMasterDumpDO.getBrand());
				makeDO.setUpdatedDate(vehicleMasterDumpDO.getUpdatedDate());
			} else {
				makeDO = new MakeDO();
				makeDO.setMakeCd(vehicleMasterDumpDO.getBrand());
				makeDO.setUpdatedDate(vehicleMasterDumpDO.getUpdatedDate());
			}
			makeRepository.save(makeDO);
			MakeModelDO makeModelDO = makeModelRepository.findByModelCdAndSeatingCapacity(vehicleMasterDumpDO.getModel(),vehicleMasterDumpDO.getSeat());
			if (makeModelDO != null) {
				// update values
				makeModelDO.setStatusCd(vehicleMasterDumpDO.getStatusCd());
				makeModelDO.setGuid(vehicleMasterDumpDO.getGuid());
				makeModelDO.setMakeCd(vehicleMasterDumpDO.getBrand());
				makeModelDO.setModelCd(vehicleMasterDumpDO.getModel());
				makeModelDO.setSeatingCapacity(vehicleMasterDumpDO.getSeat());
				makeModelDO.setUpdatedDate(vehicleMasterDumpDO.getUpdatedDate());
			} else {
				// create new object
				makeModelDO = new MakeModelDO();
				makeModelDO.setStatusCd(vehicleMasterDumpDO.getStatusCd());
				makeModelDO.setGuid(vehicleMasterDumpDO.getGuid());
				makeModelDO.setMakeCd(vehicleMasterDumpDO.getBrand());
				makeModelDO.setModelCd(vehicleMasterDumpDO.getModel());
				makeModelDO.setSeatingCapacity(vehicleMasterDumpDO.getSeat());
				makeModelDO.setUpdatedDate(vehicleMasterDumpDO.getUpdatedDate());
			}
			makeModelRepository.save(makeModelDO);
			ModelVariantDO modelVariant = modelVariantRepository.findByModelCdAndSeatingCapacityAndVariantCd(vehicleMasterDumpDO.getModel(),vehicleMasterDumpDO.getSeat(),vehicleMasterDumpDO.getVariant());
			if (modelVariant != null) {
				// update values
				modelVariant.setGuid(vehicleMasterDumpDO.getGuid());
				modelVariant.setMakeCd(vehicleMasterDumpDO.getBrand());
				modelVariant.setModelCd(vehicleMasterDumpDO.getModel());
				modelVariant.setStatusCd(vehicleMasterDumpDO.getStatusCd());
				modelVariant.setVariantCd(vehicleMasterDumpDO.getVariant());
				modelVariant.setVehiclekey(vehicleMasterDumpDO.getVehicleCode());
				modelVariant.setSeatingCapacity(vehicleMasterDumpDO.getSeat());
				modelVariant.setUpdatedDate(vehicleMasterDumpDO.getUpdatedDate());
			} else {
				// create new object
				modelVariant = new ModelVariantDO();
				modelVariant.setGuid(vehicleMasterDumpDO.getGuid());
				modelVariant.setMakeCd(vehicleMasterDumpDO.getBrand());
				modelVariant.setModelCd(vehicleMasterDumpDO.getModel());
				modelVariant.setStatusCd(vehicleMasterDumpDO.getStatusCd());
				modelVariant.setVariantCd(vehicleMasterDumpDO.getVariant());
				modelVariant.setVehiclekey(vehicleMasterDumpDO.getVehicleCode());
				modelVariant.setSeatingCapacity(vehicleMasterDumpDO.getSeat());
				modelVariant.setUpdatedDate(vehicleMasterDumpDO.getUpdatedDate());
			}
			modelVariantRepository.save(modelVariant);
			SiRegtYearDO siRegtYear = siRegtYearRepository.findByModelCdAndVariantCdAndRegtYear(vehicleMasterDumpDO.getModel(),vehicleMasterDumpDO.getVariant(),vehicleMasterDumpDO.getYear());
			if (siRegtYear != null) {
				// update values
				siRegtYear.setGuid(vehicleMasterDumpDO.getGuid());
				siRegtYear.setMakeCd(vehicleMasterDumpDO.getBrand());
				siRegtYear.setModelCd(vehicleMasterDumpDO.getModel());
				siRegtYear.setRegtYear(vehicleMasterDumpDO.getYear());
				siRegtYear.setStatusCd(vehicleMasterDumpDO.getStatusCd());
				siRegtYear.setVariantCd(vehicleMasterDumpDO.getVariant());
				siRegtYear.setVehiclekey(vehicleMasterDumpDO.getVehicleCode());
				siRegtYear.setSumInsured(vehicleMasterDumpDO.getSumInsured());
				siRegtYear.setUpdatedDate(vehicleMasterDumpDO.getUpdatedDate());
			} else {
				// create new object
				siRegtYear = new SiRegtYearDO();
				siRegtYear.setGuid(vehicleMasterDumpDO.getGuid());
				siRegtYear.setMakeCd(vehicleMasterDumpDO.getBrand());
				siRegtYear.setModelCd(vehicleMasterDumpDO.getModel());
				siRegtYear.setRegtYear(vehicleMasterDumpDO.getYear());
				siRegtYear.setStatusCd(vehicleMasterDumpDO.getStatusCd());
				siRegtYear.setVariantCd(vehicleMasterDumpDO.getVariant());
				siRegtYear.setSumInsured(vehicleMasterDumpDO.getSumInsured());
				siRegtYear.setVehiclekey(vehicleMasterDumpDO.getVehicleCode());
				siRegtYear.setUpdatedDate(vehicleMasterDumpDO.getUpdatedDate());
			}
			siRegtYearRepository.save(siRegtYear);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	public MakeRepository getMakeRepository() {
		return makeRepository;
	}
	public void setMakeRepository(MakeRepository makeRepository) {
		this.makeRepository = makeRepository;
	}
	public ModelVariantRepository getModelVariantRepository() {
		return modelVariantRepository;
	}
	public void setModelVariantRepository(ModelVariantRepository modelVariantRepository) {
		this.modelVariantRepository = modelVariantRepository;
	}
	public VehicleMasterDumpRepository getVehicleMasterDumpRepository() {
		return vehicleMasterDumpRepository;
	}
	public void setVehicleMasterDumpRepository(VehicleMasterDumpRepository vehicleMasterDumpRepository) {
		this.vehicleMasterDumpRepository = vehicleMasterDumpRepository;
	}
	public SiRegtYearRepository getSiRegtYearRepository() {
		return siRegtYearRepository;
	}
	public void setSiRegtYearRepository(SiRegtYearRepository siRegtYearRepository) {
		this.siRegtYearRepository = siRegtYearRepository;
	}
	public MakeModelRepository getMakeModelRepository() {
		return makeModelRepository;
	}
	public void setMakeModelRepository(MakeModelRepository makeModelRepository) {
		this.makeModelRepository = makeModelRepository;
	}
	public DataSyncDetailsRepository getDataSyncDetailsRepository() {
		return dataSyncDetailsRepository;
	}
	public void setDataSyncDetailsRepository(DataSyncDetailsRepository dataSyncDetailsRepository) {
		this.dataSyncDetailsRepository = dataSyncDetailsRepository;
	}
	public ApplicationRepository getApplicationRepository() {
		return applicationRepository;
	}
	public void setApplicationRepository(ApplicationRepository applicationRepository) {
		this.applicationRepository = applicationRepository;
	}
}
