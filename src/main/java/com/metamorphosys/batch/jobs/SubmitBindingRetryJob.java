package com.metamorphosys.batch.jobs;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.client.RestTemplate;

import com.metamorphosys.insureconnect.dataobjects.master.ApplicationDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.EntityDocumentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.EntitySignatureDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyCoverageDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.RetryServiceDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.jpa.transaction.EntityDocumentRepository;
import com.metamorphosys.insureconnect.jpa.transaction.EntitySignatureRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PolicyRepository;
import com.metamorphosys.insureconnect.jpa.transaction.RetryServiceRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.policy.interfaceobjects.CreatePolicyIO;
import com.metamorphosys.policy.utility.SerializationUtility;

public class SubmitBindingRetryJob extends QuartzJobBean {
	
	private static final Logger log = LoggerFactory.getLogger(SubmitBindingRetryJob.class);
	
	//@Autowired
	ApplicationRepository applicationRepository;
	private PolicyRepository policyRepository;
	private RetryServiceRepository retryServiceRepository;
	private EntityDocumentRepository entityDocumentRepository;
	private EntitySignatureRepository entitySignatureRepository;

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		
		log.info("Submit binding retry job started execution at  "+new Date());
		
		List<RetryServiceDO> retryServiceDOList = retryServiceRepository.findByServiceNameAndStatusCd(InsureConnectConstants.RetryService.BINDING,
				InsureConnectConstants.Status.PENDING);
		if(retryServiceDOList!=null && retryServiceDOList.size()>0){
			for(RetryServiceDO retryServiceDO:retryServiceDOList){
				if(retryServiceDO.getNoOfRetries()<6){
					CreatePolicyIO io = new CreatePolicyIO();
					PolicyDO policyDO =policyRepository.findByPolicyNum(retryServiceDO.getServiceId());
					io.setPolicyDO(policyDO);
					//boolean isValidFlag=true;
					
					List<EntityDocumentDO> entityDocumentDOList=new ArrayList<EntityDocumentDO>();
					//List<EntityDocumentDO> entityDocumentDOList =entityDocumentRepository.findByEntityGuidAndUploadStatus(policyDO.getGuid(),InsureConnectConstants.Status.PENDING);
					List<EntityDocumentDO> entityDocumentDOList1=new ArrayList<EntityDocumentDO>();
					List<EntityDocumentDO> entityDocumentDOList2=new ArrayList<EntityDocumentDO>();
					List<EntityDocumentDO> entityDocumentDOList3=new ArrayList<EntityDocumentDO>();
					for (PolicyCoverageDO policyCoverageDO : policyDO.getPolicyCoverageList()) {
						
						if(policyCoverageDO.getPlanTypeCd().equals("BASEPLAN"))
						{
							entityDocumentDOList1 =entityDocumentRepository.findByEntityIDAndDocumentTypeCd(policyCoverageDO.getPolicyCoverageInsuredList().get(0).getOfflineGuid(),"VEHICLE");
							entityDocumentDOList2 =entityDocumentRepository.findByEntityIDAndDocumentSubTypeCd(policyCoverageDO.getPolicyCoverageInsuredList().get(0).getOfflineGuid(),"BSTK");
							entityDocumentDOList3 =entityDocumentRepository.findByEntityIDAndDocumentSubTypeCd(policyCoverageDO.getPolicyCoverageInsuredList().get(0).getOfflineGuid(),"STNK");
							break;
						}
						
					}
					List<EntityDocumentDO> entityDocumentDOList4 =entityDocumentRepository.findByEntityIDAndDocumentTypeCd(policyDO.getPolicyClientList().get(0).getOfflineGuid(),"IDPROOF");
					
					if(entityDocumentDOList1.size()>0)
					{
						entityDocumentDOList.addAll(entityDocumentDOList1);
					}
					if(entityDocumentDOList2.size()>0)
					{
						entityDocumentDOList.addAll(entityDocumentDOList2);
					}
					if(entityDocumentDOList3.size()>0)
					{
						entityDocumentDOList.addAll(entityDocumentDOList3);
					}
					if(entityDocumentDOList4.size()>0)
					{
						entityDocumentDOList.addAll(entityDocumentDOList4);
					}
					
					if(entityDocumentDOList.size()>0){
						io.setEntityDocumentDOs(entityDocumentDOList);
						for(EntityDocumentDO entityDocumentDO:entityDocumentDOList){
	
							if(entityDocumentDO.getEntityImage()!=null)
							{
								convertToDocImage(entityDocumentDO, policyDO);
							}
							entityDocumentDO.setEntityGuid(policyDO.getGuid());
							//third Party Call
							//status of attach Fail
							//	isValidFlag=false;
						}
					}
					
					List<EntitySignatureDO> entitySignatureDOList =entitySignatureRepository.findByEntityGuidAndUploadStatus(policyDO.getGuid(),InsureConnectConstants.Status.PENDING);
					if(entitySignatureDOList.size()>0){
						io.setEntitySignatureDOs(entitySignatureDOList);
						for(EntitySignatureDO entitySignatureDO:entitySignatureDOList){
	
							if(entitySignatureDO.getSignature()!=null)
							{
								convertToSignImage(entitySignatureDO, policyDO);
							}
							entitySignatureDO.setEntityGuid(policyDO.getGuid());
	
						}
					}
					thirdPartyUpload(io);
				}else {//closing if of number of retries
					retryServiceDO.setStatusCd(InsureConnectConstants.Status.FAILED);
					retryServiceRepository.save(retryServiceDO);
				}
			} //end of retryService for loop
		}
	}
	
	@Async
	private void thirdPartyUpload(CreatePolicyIO io) {
		boolean isvalid = true;
		boolean entityDocumentUploadStatus=true;
		boolean entitySignatureUpload = true;
		try{
			//third Party Service to  
			//upload images
			// Calling third party service
			
			String authToken = "";
			String systemUserId = "";
			String systemUserPwd = "";
			systemUserId = applicationRepository.findByKey(InsureConnectConstants.SystemUserCredentials.SYSTEMUSERID).getValue();
			systemUserPwd = applicationRepository.findByKey(InsureConnectConstants.SystemUserCredentials.SYSTEMUSERPWD).getValue();
			//Code added by Anna to make third party calls on 30/01/2017
			
			HttpHeaders httpHeader = new HttpHeaders();
			httpHeader.set("channel", "mobile");
			HttpEntity entityauth = new HttpEntity(httpHeader);
			String internalAuthURL = applicationRepository.findByKey(InsureConnectConstants.Path.AUTHINTERFACEPATH).getValue()
					+ "/" + systemUserId + "/" + systemUserPwd;
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> reponseObject = restTemplate.exchange(internalAuthURL, HttpMethod.GET, entityauth, String.class);
			
			if(HttpStatus.OK.equals(reponseObject.getStatusCode())) {
				HashMap<String, String> excternalAuthAPIresponse = (HashMap<String, String>) SerializationUtility.getInstance()
						.fromJson(reponseObject.getBody(), HashMap.class);
				authToken = excternalAuthAPIresponse.get("authToken");
				HttpHeaders httpHeaders = new HttpHeaders();
				httpHeaders.setContentType(MediaType.APPLICATION_JSON);
				httpHeaders.set("Authorization", authToken);
				httpHeaders.set("referenceId", io.getPolicyDO().getProposalNum());
				httpHeaders.set("agentid", io.getPolicyDO().getAgentId());
				httpHeaders.set("source", "MOBILE");
				httpHeaders.set("channel", "mobile");
				String interfaceUrl = applicationRepository.findByKey(InsureConnectConstants.Path.ENTITYINTERFACEPATH).getValue();
				if(io.getEntityDocumentDOs() != null && io.getEntityDocumentDOs().size() > 0){
					for(EntityDocumentDO entityDocumentDO:io.getEntityDocumentDOs())
					{
						if(entityDocumentDO.getEntityImage() != null){
							httpHeaders.set("documentType", "EntityDocumentDO");
							HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(entityDocumentDO), httpHeaders);

							// individual image upload call starts. 
							ResponseEntity<String> jsonObject = restTemplate.exchange(interfaceUrl,HttpMethod.POST, entity, String.class);
							if(!HttpStatus.OK.equals(jsonObject.getStatusCode())){
								entityDocumentUploadStatus = false;
								entityDocumentDO.setUploadStatus(InsureConnectConstants.Status.PENDING);
								break;
							}else{
								HashMap<String, String> hashMap = (HashMap<String, String>) SerializationUtility.getInstance().fromJson(jsonObject.getBody(), HashMap.class);
								entityDocumentDO.setReferenceID(hashMap.get("imageReferenceId"));
								entityDocumentDO.setEntityImageName(hashMap.get("imageFileName"));
								entityDocumentDO.setUploadStatus(InsureConnectConstants.Status.SUCCESS);
							}

						}	
					}
				}
				if(io.getEntitySignatureDOs() != null && io.getEntitySignatureDOs().size() > 0){
					for(EntitySignatureDO entitySignatureDO:io.getEntitySignatureDOs())
					{
						if(entitySignatureDO.getSignature() != null){
							httpHeaders.set("documentType", "EntitySignatureDO");
							HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(entitySignatureDO), httpHeaders);
							//String interfaceUrl = applicationRepository.findByKey(InsureConnectConstants.Path.ENTITYINTERFACEPATH).getValue();
							ResponseEntity<String> jsonObject = restTemplate.exchange(interfaceUrl,HttpMethod.POST, entity, String.class);
							if(!HttpStatus.OK.equals(jsonObject.getStatusCode())){
								entitySignatureUpload = false;
								entitySignatureDO.setUploadStatus(InsureConnectConstants.Status.PENDING);
								break;
							}else{
								HashMap<String, String> hashMap = (HashMap<String, String>) SerializationUtility.getInstance().fromJson(jsonObject.getBody(), HashMap.class);
								entitySignatureDO.setReferenceID(hashMap.get("imageReferenceId"));
								entitySignatureDO.setEntityImageName(hashMap.get("imageFileName"));
								entitySignatureDO.setUploadStatus(InsureConnectConstants.Status.SUCCESS);
							}
						}
					}
				}
			}else{
				isvalid = false;
			}
			
			//Based on the status of Uploded document and signature , need to  
			
			if(isvalid && entityDocumentUploadStatus && entitySignatureUpload){
				String status=submitBinding(io, authToken);
				System.out.println("STATUS OF  SUBMIT BINDING "+status);
				if(status == null || !status.equals("PASSED")){
					saveTaskForRetry(io.getPolicyDO().getPolicyNum(), "BINDING", InsureConnectConstants.Status.PENDING);
				}else{
					saveTaskForRetry(io.getPolicyDO().getPolicyNum(), "BINDING", InsureConnectConstants.Status.SUCCESS);
				}
			}else{
				saveTaskForRetry(io.getPolicyDO().getPolicyNum(), "BINDING", InsureConnectConstants.Status.SUCCESS);
			}
			
			
		}catch(Exception e){
			saveTaskForRetry(io.getPolicyDO().getPolicyNum(), "BINDING", InsureConnectConstants.Status.PENDING);
			e.printStackTrace();
		}
		
	}
	
	private void saveTaskForRetry(String serviceId, String serviceName, String serviceStatus){


		RetryServiceDO retryServiceDO =retryServiceRepository.findByServiceId(serviceId);
		if(retryServiceDO!=null)
		{
			retryServiceDO.setServiceId(serviceId);
			retryServiceDO.setServiceName(serviceName);
			retryServiceDO.setStatusCd(serviceStatus);
			retryServiceDO.setNoOfRetries(retryServiceDO.getNoOfRetries()+1);
			retryServiceRepository.save(retryServiceDO);

		}

	}

	private String submitBinding(CreatePolicyIO io, String authToken) {
		try{
			
			//third Party Service for submit binding
			//Anna Make Change on 31/1/17
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			httpHeaders.set("Authorization", authToken);
			// For corporate policy there is abug.Set firstname to some blank string before submitting else it will be submitted as null
			if(io.getPolicyDO().getPolicyClientList().get(0).getClientTypeCd().equals("corporate"))
			{
				io.getPolicyDO().getPolicyClientList().get(0).setFirstName("");
			}
			HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(io.getPolicyDO()), httpHeaders);
			String interfaceUrl = applicationRepository.findByKey(InsureConnectConstants.Path.POLICYBINDINGDPATH).getValue();
			if(io.getPolicyDO().getBaseProductFamilyCd().equals("ALI")) {
				interfaceUrl = applicationRepository.findByKey(InsureConnectConstants.Path.ALIPOLICYBINDINGDPATH).getValue();
			}
			
			//http://localhost:8091/interface/policyInterface
			
			ResponseEntity<String> jsonObject = restTemplate.exchange(interfaceUrl,HttpMethod.POST, entity, String.class);
			HashMap<String, String> resultHashMap = (HashMap<String, String>) SerializationUtility.getInstance().fromJson(jsonObject.getBody(), HashMap.class);
			return resultHashMap.get("submitBindingStatus");
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
		
	}

	public PolicyRepository getPolicyRepository() {
		return policyRepository;
	}

	public void setPolicyRepository(PolicyRepository policyRepository) {
		this.policyRepository = policyRepository;
	}

	public RetryServiceRepository getRetryServiceRepository() {
		return retryServiceRepository;
	}

	public void setRetryServiceRepository(RetryServiceRepository retryServiceRepository) {
		this.retryServiceRepository = retryServiceRepository;
	}

	public EntityDocumentRepository getEntityDocumentRepository() {
		return entityDocumentRepository;
	}

	public void setEntityDocumentRepository(EntityDocumentRepository entityDocumentRepository) {
		this.entityDocumentRepository = entityDocumentRepository;
	}

	public EntitySignatureRepository getEntitySignatureRepository() {
		return entitySignatureRepository;
	}

	public void setEntitySignatureRepository(EntitySignatureRepository entitySignatureRepository) {
		this.entitySignatureRepository = entitySignatureRepository;
	}
	
	//Swapnil Kate : all required functions for images part.
	
	public void convertToDocImage(EntityDocumentDO entityDocumentDO, PolicyDO policyDO) {
		try {

			// Converting a Base64 string to image byte array
			String docImage = entityDocumentDO.getEntityImage().substring(entityDocumentDO.getEntityImage().indexOf(",")+1);
			String imageType = getImageType(entityDocumentDO.getEntityImage());
			byte[] imageByteArray = decodeImage(docImage);
			entityDocumentDO.setEntityImageMimeType(imageType);
			String fileName="";
			if(entityDocumentDO.getDocumentSubTypeCd().trim().equalsIgnoreCase("IDENTITY")) {
				fileName=  entityDocumentDO.getDocumentTypeCd()+"_"+convertTODate(policyDO.getPolicyCommencementDt())+"_"+entityDocumentDO.getId()+"."+imageType;
			}else if(entityDocumentDO.getDocumentTypeCd().trim().equalsIgnoreCase("ACCESSORYIMAGE")) {
				fileName=  "VEHICLE_ACCESSORY_"+entityDocumentDO.getReferenceTypeCd()+"_"+convertTODate(policyDO.getPolicyCommencementDt())+"_"+entityDocumentDO.getId()+"."+imageType;
			}	else {
				fileName=  entityDocumentDO.getDocumentSubTypeCd()+"_"+convertTODate(policyDO.getPolicyCommencementDt())+"_"+entityDocumentDO.getId()+"."+imageType;
			}
			entityDocumentDO.setEntityImageName(fileName);
			ApplicationDO appDO=applicationRepository.findByKey(InsureConnectConstants.Path.ENTITYDOCPATH);
			String path = appDO.getValue() +"/"+ policyDO.getProposalRefNum()+"/"+fileName;
//			String path = "D:/VehicleImage";
			entityDocumentDO.setImageURL(path);
//			entityDocumentDO.setReferenceID("2000");
			imageCreation(path, imageByteArray, fileName);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void convertToSignImage(EntitySignatureDO entitySignatureDO, PolicyDO policyDO) {
		try {

			// Converting a Base64 string to image byte array
			String signature = entitySignatureDO.getSignature().substring(entitySignatureDO.getSignature().indexOf(",")+1);
			String imageType = getImageType(entitySignatureDO.getSignature());
			entitySignatureDO.setEntityImageMimeType(imageType);
			byte[] imageByteArray = decodeImage(signature);
			
			String fileName=  entitySignatureDO.getEntitySignatureID()+"_"+convertTODate(policyDO.getPolicyCommencementDt())+entitySignatureDO.getEntityID()+"."+imageType;
			entitySignatureDO.setEntityImageName(fileName);
			ApplicationDO appDO=applicationRepository.findByKey(InsureConnectConstants.Path.ENTITYDOCPATH);
			String path = appDO.getValue() +"/"+ policyDO.getProposalRefNum()+"/"+entitySignatureDO.getReferenceTypeCd()+"/"+fileName;
			entitySignatureDO.setImageURL(path);
			imageCreation(path, imageByteArray, fileName);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void imageCreation(String path,byte[] imageByteArray, String fileName){
		try{
			File directory = new File(String.valueOf(path));
		    if (! directory.getParentFile().exists()){
		        directory.getParentFile().mkdir();
		    }		
			// Write a image byte array into file system
			FileOutputStream fileOutputStream = new FileOutputStream(directory);
			fileOutputStream.write(imageByteArray);
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static byte[] decodeImage(String imageString) {
		return Base64.decodeBase64(imageString);
	}
	
	public String getImageType(String signature){
		String imageType = "jpeg";
		imageType = signature.substring(0, signature.indexOf(";"));
		imageType = imageType.substring(imageType.indexOf("/")+1);
		return imageType;
	}
	
	public String convertTODate(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String format = formatter.format(date);
		return format;
	}
	public ApplicationRepository getapApplicationRepository()
	{
		return applicationRepository;
	}

	public void setApplicationRepository(ApplicationRepository applicationRepository)
	{
		this.applicationRepository=applicationRepository;
	}
}
