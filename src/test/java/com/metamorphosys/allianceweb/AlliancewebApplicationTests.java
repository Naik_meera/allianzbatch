package com.metamorphosys.allianceweb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.metamorphosys.allianceWeb.AlliancewebApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AlliancewebApplication.class)
public class AlliancewebApplicationTests {

	@Test
	public void contextLoads() {
	}

}
